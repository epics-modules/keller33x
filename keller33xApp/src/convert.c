#include <stdlib.h>
#include <stdio.h>
#include <aSubRecord.h>
#include <math.h>

#include "convert.h"


char compare_arrays(unsigned char * a, unsigned char * b, int size) {
    int i;
    for (i = 0; i < size; i++)
        if (a[i] != b[i])
            return 1;

    return 0;
}

double convert(unsigned char input[4]) {
    long int input_comp, sig, man, expo;
    double result;

    input_comp  = (int) (input[3] + (input[2] << 8) + (input[1] << 16) + (input[0] << 24));
    sig  = (input_comp >> 32)& 1;
    man  = (input_comp >> 23) & 255;
    expo = (input_comp) & 8388607;
    result = (double) pow(-1,sig)*(1.0 + (double) expo/8388608)*(pow(2,man-127));
    return result;
}

/*
 * Check for errors the input and convert
 * to the rigth format
 * 
 * Parameters:
 *  INA: Input waveform (read from keller33x)
 *  INB: Size of Input Waveform
 *
 * Output:
 *  OUT: Valid/Invalid (0: Valid / 1: Invalid)
 *  VALA: Result
 */
static long check_and_convert(aSubRecord *psr)
{
    unsigned char * in_data     = (unsigned char*)psr->a;
    long  in_data_len = *((long*)psr->b);
    unsigned char pre_error[4] = {1, 161, 167, 250};
    unsigned char temp_error[4] = {4, 162, 103, 250};
    unsigned char data[4];
    int i;
    
    if (in_data_len < 9)
        return 1;

    if (in_data[0] != 1 && in_data[1] != 3) // Start with wrong values
        return 1;

    if (compare_arrays(&in_data[3], pre_error, 4) == 0 || compare_arrays(&(in_data[3]), temp_error, 4) == 0)
        return 1;

    //TODO: check CRC?
    
    //Copy only the data part
    for (i = 0; i < 4; i++){
        data[i] = in_data[i + 3];
    }

    *((double*)psr->vala) = convert(data);

    return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(check_and_convert);
