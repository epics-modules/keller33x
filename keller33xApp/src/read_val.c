#include <stdlib.h>
#include <stdio.h>
#include <aSubRecord.h>

/*
 * Read Temperature
 *
 * Output:
 *  VALA: Waveform to Keller33x
 */
static long read_temp(aSubRecord *psr)
{
    int i;
    char temp_array[8] = {1, 3, 0, 8, 0, 2, 69, 201};
    char * output = (char*)psr->vala;

    for(i = 0 ; i < 8 ; i ++) {
        output[i] = temp_array[i];
    }

    psr->neva = 8;
    return 0;
}


/*
 * Read Pressure 
 *
 * Output:
 *  VALA: Waveform to Keller33x
 */
static long read_press(aSubRecord *psr)
{
    int i;
    char press_array[8] = {1, 3, 0, 2, 0, 2, 101, 203};
    char * output = (char*)psr->vala;

    for(i = 0 ; i < 8 ; i ++) {
        output[i] = press_array[i];
    }

    psr->neva = 8;
    return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(read_temp);
epicsRegisterFunction(read_press);
